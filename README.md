# Clojure microservices

A small app based on the container shipping domain from the Domain Driven Design book and the go-kit implementation


* https://github.com/citerus/dddsample-core
* https://github.com/go-kit/kit/tree/master/examples/shipping


## Run all tests

```
lein monolith each do clean, test
```


## Services

* Booking (the only one that currently has any code)
* Handling
* Tracking
