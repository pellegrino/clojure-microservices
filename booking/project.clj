(defproject booking "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :deployable true
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/core.match "0.3.0-alpha5"]
                 [org.clojure/core.specs.alpha "0.2.36"]
                 [mount "0.1.12"]
                 [org.clojure/tools.logging "0.4.1"]
                 ; GRPC
                 [com.google.protobuf/protobuf-java "3.6.0"]
                 [javax.annotation/javax.annotation-api "1.2"]
                 [io.netty/netty-codec-http2 "4.1.25.Final"]
                 [io.grpc/grpc-core "1.14.0"]
                 [io.grpc/grpc-netty "1.14.0"
                  :exclusions [io.grpc/grpc-core
                               io.netty/netty-codec-http2]]
                 [io.grpc/grpc-protobuf "1.14.0"]
                 [io.grpc/grpc-stub "1.14.0"]]
  :plugins [[lein-protoc "0.4.2"]
            [lein-cljfmt "0.6.0"]]
  :target-path "target/%s"
  :main ^:skip-aot booking.core
  :proto-source-paths ["resources/proto"]
  :protoc-version "3.6.0"
  :protoc-grpc {:version "1.14.0"}
  :proto-target-path "target/generated-sources/protobuf"
  :java-source-paths ["target/generated-sources/protobuf"]
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[org.clojure/test.check "0.10.0-alpha3"]]}})
