(ns booking.client
  (:import (io.grpc ManagedChannelBuilder)
           (booking.grpc FizzBuzzGrpc MessageTypes$Request)))

(defn create-conn [^String host port]
  (-> (ManagedChannelBuilder/forAddress host port)
      (.usePlaintext true)
      .build
      (FizzBuzzGrpc/newBlockingStub)))

(defn fizzbuzz ^String [conn n]
  (let [request (-> (MessageTypes$Request/newBuilder)
                    (.setN n)
                    .build)]
    (-> (.fizzBuzz conn request)
        (.getMessage))))
