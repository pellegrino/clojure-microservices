(ns booking.service
  (:require [clojure.core.match :refer [match]]
            [clojure.spec.alpha :as spec])
  (:import [booking.grpc FizzBuzzGrpc$FizzBuzzImplBase
            MessageTypes$Response
            MessageTypes$Request]))

(defn fizzbuzz ^String [n]
  (match [(mod n 3) (mod n 5)]
    [0 0] "FizzBuzz"
    [0 _] "Fizz"
    [_ 0] "Buzz"
    :else (str n)))

(spec/fdef fizzbuzz
           :args (spec/cat :n (spec/and integer? pos?))
           :ret string?
           :fn #(not (= "" (:ret %))))

(defn create-service
  "Creates a new booking service that iumplements the gRPC stubs"
  []
  (proxy [FizzBuzzGrpc$FizzBuzzImplBase] []
    (fizzBuzz [^MessageTypes$Request request response-observer]
      (let [n (.getN request)
            builder (-> (MessageTypes$Response/newBuilder)
                        (.setMessage (fizzbuzz n)))]
        (.onNext response-observer (.build builder))
        (.onCompleted response-observer)))))