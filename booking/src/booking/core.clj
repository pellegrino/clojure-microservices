(ns booking.core
  (:require [booking.service :as service]
            [mount.core :as mount]
            [clojure.tools.logging :as log])
  (:import [io.grpc ServerBuilder])
  (:gen-class))

(def server-atom (atom nil))

(mount/defstate ^{:on-reload :noop} grpc-server
  :start
  (let [builder (ServerBuilder/forPort (read-string (or (System/getenv "PORT") "35000")))
        service (service/create-service)
        _ (.addService builder service)
        server (.build builder)]
    (.start server)
    (log/info "Server started on port 35000")
    (reset! server-atom server)
    (.awaitTermination server))
  :stop
  (.shutdown @server-atom))

(defn -main
  "Starts the booking gRPC server"
  [& args]
  (mount/start))
